FROM node:8-alpine 
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --save express
COPY . .
EXPOSE 5000
CMD ["npm", "start"]